package com.example.sqlitefotbol;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {


    //create a new instance, takes one argument context this, constructor will create data base and table
    //create an instance of the class with instance name myDb
    DatabaseHelper myDb;
    //Define 3 variables for Edit Text and one variable for Button

    private static final int NUM_TEAMS = 3;
    public static final String EXTRA_MESSAGE = "com.example.sqlitefotbol.extra.MESSAGE";

    Context ctx = this;

    static ArrayList<Team> favouriteTeams;  // 3 valda favoritlag
    static ArrayList<FavouriteTeamWrapper> favouriteTeamWrappers;
    static Spinner spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        favouriteTeams = new ArrayList<>();
        favouriteTeamWrappers = new ArrayList<FavouriteTeamWrapper>();

        AsyncTaskRunner myAsyncTask = new AsyncTaskRunner();
        myAsyncTask.execute();
    }

    public void launchAddTeamActivity(String viewId){

        int index = getFavouriteTeamsIndexByViewId(viewId);
        String teamName = favouriteTeamWrappers.get(index).getTeam().getName();

        Intent intent = new Intent(this, AddTeamActivity.class);
        Bundle extras = new Bundle();
        extras.putString("VIEW_ID", viewId);
        extras.putString("TEAM_NAME", teamName);
        intent.putExtras(extras);
        startActivity(intent);
    }

    private void launchTeamDataActivity(String viewId) {

        Intent intent = new Intent(this, TeamDataActivity.class);

        int favTeamIndex = getFavouriteTeamsIndexByViewId(viewId);
        String teamId = Integer.toString((favouriteTeamWrappers.get(favTeamIndex).getTeam().getId()));

        Bundle extras = new Bundle();
        extras.putString("VIEW_ID",viewId);
        extras.putString("TEAM_ID",teamId);
        intent.putExtras(extras);
        startActivity(intent);
    }

    private void  launchMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    private int getFavouriteTeamsIndexByViewId(String viewId) {
        int index = (Integer.parseInt(viewId.substring((viewId.length()-1))))-1;
        return index;
    }

    private int getDatabaseRowByViewId(String viewId) {
        int index = (Integer.parseInt(viewId.substring((viewId.length()-1))));
        return index;
    }

    private void initDatabase() {
        myDb.insertData("NO CHOSEN TEAM", 0);
        myDb.insertData("NO CHOSEN TEAM", 0);
        myDb.insertData("NO CHOSEN TEAM", 0);
    }

    private class AsyncTaskRunner extends AsyncTask<Void, Void, String> {

        private String resp;

        @Override
        protected String doInBackground(Void... voids) {

           try{
               myDb = new DatabaseHelper(ctx);

               // Kollar om databasen är tom. I så fall fyll tre rader med NO CHOSEN TEAM
               Team teamVerifyingDb = myDb.readData(1);
               if (teamVerifyingDb == null) {
                   initDatabase();
               }

               for(int i = 0; i < NUM_TEAMS; i++) {

                   Team teamFromDb = myDb.readData(i+1);
                   FavouriteTeamWrapper ftw = new FavouriteTeamWrapper(teamFromDb);

                   favouriteTeamWrappers.add(ftw);
               }
           } catch (Exception e) {
               e.printStackTrace();
               resp = e.getMessage();
           }

           return resp;
        }

        //@Override
        protected void onPostExecute(String result) {
            // execution of result of Long time consuming operation
            for(int i = 0; i < NUM_TEAMS; i++) {

                FavouriteTeamWrapper ftw = favouriteTeamWrappers.get(i);

                TextView tw = findViewById(getResources().getIdentifier("textView_teamName" + (i+1), "id", getPackageName()));
                ftw.setTextViewTeamName(tw);

                ImageView ivAdd = findViewById(getResources().getIdentifier("buttonAdd" + (i+1), "id", getPackageName()));
                ftw.setImageViewAddButton(ivAdd);

                ImageView ivEdit = findViewById(getResources().getIdentifier("buttonEdit" + (i+1), "id", getPackageName()));
                ftw.setImageViewEditButton(ivEdit);

                ImageView ivDelete = findViewById(getResources().getIdentifier("buttonDelete" + (i+1), "id", getPackageName()));
                ftw.setImageViewDeleteButton(ivDelete);

                String teamName = ftw.getTeam().getName();
                if (teamName.equals("NO CHOSEN TEAM")) {
                    ivDelete.setVisibility(View.INVISIBLE);
                    ivEdit.setVisibility(View.INVISIBLE);
                } else ivAdd.setVisibility(View.INVISIBLE);
                tw.setText(ftw.getTeam().getName());
            }


            for(int j = 0; j < NUM_TEAMS; j++) {
                favouriteTeamWrappers.get(j)
                        .getTextViewTeamName()
                        .setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                String viewId = v.getResources().getResourceEntryName(v.getId());
                                int index = getFavouriteTeamsIndexByViewId(viewId);
                                String teamName = favouriteTeamWrappers.get(index).getTeam().getName();
                                if(!teamName.equals("NO CHOSEN TEAM")) launchTeamDataActivity(viewId);
                            }
                        });

                favouriteTeamWrappers.get(j).getImageViewAddButton().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String viewId = v.getResources().getResourceEntryName(v.getId());
                        launchAddTeamActivity(viewId);
                    }
                });

                favouriteTeamWrappers.get(j).getImageViewDeleteButton().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String viewId = v.getResources().getResourceEntryName(v.getId());
                        int id = getDatabaseRowByViewId(viewId);
                        myDb.updateData(id, "NO CHOSEN TEAM", 0);
                        launchMainActivity();
                    }
                });

                favouriteTeamWrappers.get(j).getImageViewEditButton().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String viewId = v.getResources().getResourceEntryName(v.getId());
                        launchAddTeamActivity(viewId);
                    }
                });
            }
        }
    }
}
