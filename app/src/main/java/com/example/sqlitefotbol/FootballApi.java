package com.example.sqlitefotbol;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface FootballApi {

    @Headers("X-Auth-Token: 5ba8dc0005f14925bbb29bd9ede7aa0a")
    @GET ("/v2/competitions/{id}/teams")
    Call<CompetitionWithTeams>getTeamByCompetitionId(@Path("id") String id);

    @Headers("X-Auth-Token: 5ba8dc0005f14925bbb29bd9ede7aa0a")
    @GET ("/v2/teams/{id}")
    Call<Team>getTeamById(@Path("id") String id);

    @Headers("X-Auth-Token: 5ba8dc0005f14925bbb29bd9ede7aa0a")
    @GET ("/v2/competitions/{id}/standings")
    Call<StandingList>getStandingListById(@Path("id") String id);
}

