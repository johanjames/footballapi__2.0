package com.example.sqlitefotbol;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Season {

    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("startDate")
    @Expose
    public String startDate;
    @SerializedName("endDate")
    @Expose
    public String endDate;
    @SerializedName("currentMatchday")
    @Expose
    public Object currentMatchday;
    @SerializedName("availableStages")
    @Expose
    public List<String> availableStages = null;

}