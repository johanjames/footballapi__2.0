package com.example.sqlitefotbol;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Team {

    @SerializedName("id")
    @Expose
    public Integer id;

    public int position;
    public int playedGames;
    public int points;
    public int goalsFor;
    public int goalsAgainst;


    // Id in SQLite database
    @SerializedName("dbId")
    @Expose
    public Integer dbId;

    @SerializedName("area")
    @Expose
    public Area area;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("shortName")
    @Expose
    public String shortName;
    @SerializedName("tla")
    @Expose
    public String tla;
    @SerializedName("crestUrl")
    @Expose
    public String crestUrl;
    @SerializedName("address")
    @Expose
    public String address;
    @SerializedName("phone")
    @Expose
    public String phone;
    @SerializedName("website")
    @Expose
    public String website;
    @SerializedName("email")
    @Expose
    public String email;
    @SerializedName("founded")
    @Expose
    public Integer founded;
    @SerializedName("clubColors")
    @Expose
    public String clubColors;
    @SerializedName("venue")
    @Expose
    public String venue;
    @SerializedName("lastUpdated")
    @Expose
    public String lastUpdated;

    @SerializedName("squad")
    @Expose
    public List<Squad> squad = null;

    public Team() {
        this.name = "NAME UNSET";
        this.id = 0;
    }

    public Team(String teamName, int teamId) {
        this.name = teamName;
        this.id = teamId;
    }

    public int getPosition() {
        return position;
    }

    public int getPlayedGames() {
        return playedGames;
    }

    public int getPoints() {
        return points;
    }

    public int getGoalsFor() {
        return goalsFor;
    }

    public int getGoalsAgainst() {
        return goalsAgainst;
    }

    public Integer getId() {
        return id;
    }

    public Area getArea() {
        return area;
    }

    public String getName() {
        return name;
    }

    public String getShortName() {
        return shortName;
    }

    public String getTla() {
        return tla;
    }

    public String getCrestUrl() {
        return crestUrl;
    }

    public String getAddress() {
        return address;
    }

    public String getPhone() {
        return phone;
    }

    public String getWebsite() {
        return website;
    }

    public String getEmail() {
        return email;
    }

    public Integer getFounded() {
        return founded;
    }

    public String getClubColors() {
        return clubColors;
    }

    public String getVenue() {
        return venue;
    }

    public String getLastUpdated() {
        return lastUpdated;
    }

    public Integer getDbId() {
        return dbId;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public void setPlayedGames(int playedGames) {
        this.playedGames = playedGames;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public void setGoalsFor(int goalsFor) {
        this.goalsFor = goalsFor;
    }

    public void setGoalsAgainst(int goalsAgainst) {
        this.goalsAgainst = goalsAgainst;
    }

    public void setArea(Area area) {
        this.area = area;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public void setTla(String tla) {
        this.tla = tla;
    }

    public void setCrestUrl(String crestUrl) {
        this.crestUrl = crestUrl;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setFounded(Integer founded) {
        this.founded = founded;
    }

    public void setClubColors(String clubColors) {
        this.clubColors = clubColors;
    }

    public void setVenue(String venue) {
        this.venue = venue;
    }

    public void setLastUpdated(String lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public void setDbId(Integer dbId) {
        this.dbId = dbId;
    }

    public List<Squad> getSquad() {
        return squad;
    }
}