package com.example.sqlitefotbol;

import com.google.gson.annotations.SerializedName;

public class Area {

    @SerializedName("id")
    public Integer id;

    @SerializedName("name")
    public String name;

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}