package com.example.sqlitefotbol;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CompetitionWithTeams {

    @SerializedName("count")
    @Expose
    public Integer count;

    //@SerializedName("filters")
    //@Expose
    //public Filters filters;

    @SerializedName("competition")
    @Expose
    public Competition competition;

    @SerializedName("season")
    @Expose
    public Season season;

    @SerializedName("teams")
    @Expose
    public List<Team> teams = null;

    public Integer getCount() {
        return count;
    }

    public Competition getCompetition() {
        return competition;
    }

    public Season getSeason() {
        return season;
    }

    public List<Team> getTeams() {
        return teams;
    }
}