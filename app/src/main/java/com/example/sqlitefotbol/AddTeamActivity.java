package com.example.sqlitefotbol;

import androidx.appcompat.app.AppCompatActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
//import androidx.appcompat.widget.AppCompatSpinner;
import android.widget.Toast;

import java.util.ArrayList;

//we can do test för api level 29 med androidx.appcompat.widget.AppCompatSpinner instead of android.widget.Spinner

public class AddTeamActivity extends AppCompatActivity implements
        AdapterView.OnItemSelectedListener{

    Context ctx = this;
    static ArrayList<String> teamNames;
    static ArrayList<Integer> teamIds;
    static Spinner spinner1 = null;
    //static AppCompatSpinner spinner1 = null;
    Button buttonOk;
    String selectedTeamName;
    int selectedTeamId;
    DatabaseHelper myDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_team);

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        final String VIEW_ID = extras.getString("VIEW_ID");
        final String TEAM_NAME = extras.getString("TEAM_NAME");

        myDb = new DatabaseHelper(this);

        buttonOk = findViewById(R.id.button_add_team_ok);
        buttonOk.setEnabled(false);

        buttonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // extraherar siffran på slutet från tex 'addbutton1' och 'editbutton2'
                // för att sen användas som id i anrop till databas.
                int id = Integer. parseInt(VIEW_ID.substring(VIEW_ID.length() - 1));

                AsyncTaskRunner2 myAsyncTask = new AsyncTaskRunner2();
                myAsyncTask.execute(id);
            }
        });

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://api.football-data.org/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        FootballApi footballApi =
                retrofit.create(FootballApi.class);

        // Create the spinner.
        spinner1 = findViewById(R.id.spinner1);
        if (spinner1 != null) {
            spinner1.setOnItemSelectedListener(this);
        }

        Call<CompetitionWithTeams> call1 = footballApi.getTeamByCompetitionId("2021");
        call1.enqueue(new Callback<CompetitionWithTeams>() {

            @Override
            public void onResponse(Call<CompetitionWithTeams> call1, Response<CompetitionWithTeams> response) {

                if (!response.isSuccessful()) {
                    return;
                }

                CompetitionWithTeams compWithTeams = response.body();
                ArrayList<Team> teamList;
                teamList = (ArrayList)compWithTeams.getTeams();
                teamNames = new ArrayList<>();
                teamIds = new ArrayList<>();
                for(Team team : teamList) {
                    teamIds.add(team.getId());
                    teamNames.add(team.getName());
                }

                ArrayAdapter<String> adapter = new ArrayAdapter<>(ctx, R.layout.simple_list_item_1, teamNames);

                // Apply the adapter to the spinner.
                if (spinner1 != null) {
                    spinner1.setAdapter(adapter);
                    if(!TEAM_NAME.equals("NO CHOSEN TEAM")) {
                        spinner1.setSelection(adapter.getPosition(TEAM_NAME));
                    }
                }
            }

            @Override
            public void onFailure(Call<CompetitionWithTeams> call1, Throwable t) {
                Context context = getApplicationContext();
                CharSequence text = "Fel: " + t.getMessage();
                int duration = Toast.LENGTH_LONG;

                Toast toast = Toast.makeText(context, text, duration);
                toast.show();
            }
        });
    }

    private void  launchMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

        selectedTeamName = adapterView.getItemAtPosition(i).toString();
        selectedTeamId = teamIds.get(i);
        buttonOk.setEnabled(true);
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
        buttonOk.setEnabled(false);
    }

    private class AsyncTaskRunner2 extends AsyncTask<Integer, String, String> {

        @Override
        protected String doInBackground(Integer... integers) {

            myDb.updateData(integers[0], selectedTeamName, selectedTeamId);
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            launchMainActivity();
        }
    }
}
