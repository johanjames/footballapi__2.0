package com.example.sqlitefotbol;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

import java.util.concurrent.ExecutionException;

public class DatabaseHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "Team.db";
    public static final String TABLE_NAME = "team_table";
    public static final String COLUMN_1 = "ID";
    public static final String COLUMN_2 = "FAVORITE_FOOTBALL_CLUB";
    public static final String COLUMN_3 = "CLUB_ID";

    //constructor to create data base and table
    public DatabaseHelper(Context context){
        //super class
        super (context, DATABASE_NAME, null, 1);

        SQLiteDatabase db = this.getWritableDatabase();
    }

    @Override
    public void onCreate (SQLiteDatabase db){
        //it takes the string variable or string query
        //db.execSQL("create table " + TABLE_NAME +" ("+ COLUMN_1+")" )
        //create a table using this query, table with 4 columns, id increments automatically if you do not give data to it, type: integer, text
        db.execSQL("create table " + TABLE_NAME +" (ID INTEGER PRIMARY KEY AUTOINCREMENT, FAVORITE_FOOTBALL_CLUB TEXT   , CLUB_ID INTEGER)" );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    public void createTable() {
        SQLiteDatabase db = this.getWritableDatabase();
        onCreate(db);
    }

    public void clearDatabase() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
    }

    //method to insert data into the table
    //method insert: @return the row ID of the newly inserted row, or -1 if an error occurred
    public boolean insertData(String teamName, int teamId){
        //create instance of SQLite database in our insert data method
        SQLiteDatabase db = this.getWritableDatabase();
        //content values to put some data into the columns
        ContentValues contentValues = new ContentValues();
        //put method accepts two parameters: string key , byte value that we will pass
        //contentValues.put(COLUMN_1, id);
        contentValues.put(COLUMN_2, teamName);
        contentValues.put(COLUMN_3, teamId);
        //insert the values using db instance, contentValues in the table TABLE_NAME, insert method with 4 arguments in class SQLiteDatabase.java
        //in case of error returns -1
        //Go To Declaration to find the method , rigth click
        //if the data is not inserted the method insert will return -1

        long result = db.insert(TABLE_NAME, null, contentValues);
        if (result == -1){
            return false;
        }else
            return true;
    }

    // Metoden används även för att lägga in nytt Team i DB.
    public boolean updateData(int id, String teamName, int teamId){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues args = new ContentValues();
        try {
            args.put(COLUMN_2, teamName);
            args.put(COLUMN_3, teamId);
        } catch(Exception e) {
            Log.d("DatabaseHelper", "Error while trying to update post in DB");
        }
        return db.update(TABLE_NAME, args, COLUMN_1 + "=" + id, null) > 0;
    }

    public Team readData(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        String name;
        int teamId;
        Team team0 = null;

        try {
            Cursor resultSet = db.rawQuery("Select * from " + TABLE_NAME + " Where id=" + id,null);
            resultSet.moveToFirst();
            name = resultSet.getString(1);
            teamId = resultSet.getInt(2);

            team0 = new Team();
            team0.setName(name);
            team0.setId(teamId);
        } catch (Exception e) {
            Log.d("DatabaseHelper", "Error while trying to read from DB ###########################################################################################");

        }
        return team0;
    }
}
