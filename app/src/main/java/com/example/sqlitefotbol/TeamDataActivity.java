package com.example.sqlitefotbol;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ahmadrosid.svgloader.SvgLoader;
import java.util.ArrayList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class TeamDataActivity extends AppCompatActivity {

    ImageView img;
    Activity activity = this;
    Team team;
    TextView tw0, tw1, tw2, tw3, tw4, tw5;
    String t0, t1, t2, t3, t4, t5, url_svg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team_data);

        tw0 = findViewById(R.id.textView_team_name);
        tw1 = findViewById(R.id.textView_data_1);
        tw2 = findViewById(R.id.textView_data_2);
        tw3 = findViewById(R.id.textView_data_3);
        tw4 = findViewById(R.id.textView_data_4);
        tw5 = findViewById(R.id.textView_data_5);
        img = findViewById(R.id.Iv_image_from_url);

        if (savedInstanceState != null) {
            tw0.setText(savedInstanceState.getString("tw0"));
            tw1.setText(savedInstanceState.getString("tw1"));
            tw2.setText(savedInstanceState.getString("tw2"));
            tw3.setText(savedInstanceState.getString("tw3"));
            tw4.setText(savedInstanceState.getString("tw4"));
            tw5.setText(savedInstanceState.getString("tw5"));
            url_svg = savedInstanceState.getString("url_svg");
            SvgLoader.pluck()
                    .with(activity)
                    .setPlaceHolder(R.mipmap.ic_launcher, R.mipmap.ic_launcher)
                    .load(url_svg, img);
        }

        team = new Team();

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        final String viewId = extras.getString("VIEW_ID"); // För framtida bruk
        final String teamId = extras.getString("TEAM_ID");

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://api.football-data.org/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        FootballApi footballApi =
                retrofit.create(FootballApi.class);

        // Anrop med Retrofit för att hämta lagets flagga
        Call<Team> call1 = footballApi.getTeamById(teamId);
        call1.enqueue(new Callback<Team>() {

            @Override
            public void onResponse(Call<Team> call1, Response<Team> response) {

                if (!response.isSuccessful()) {
                    return;
                }
                Team team = response.body();

                url_svg = team.getCrestUrl();

                // Ladda .svg-fil från URL
                SvgLoader.pluck()
                        .with(activity)
                        .setPlaceHolder(R.mipmap.ic_launcher, R.mipmap.ic_launcher)
                        .load(url_svg, img);
            }

            @Override
            public void onFailure(Call<Team> call1, Throwable t) {
                Context context = getApplicationContext();
                CharSequence text = "Fel: " + t.getMessage();
                int duration = Toast.LENGTH_LONG;

                Toast toast = Toast.makeText(context, text, duration);
                toast.show();            }
        });

        Call<StandingList> call2 = footballApi.getStandingListById("2021");
        call2.enqueue(new Callback<StandingList>() {

            @Override
            public void onResponse(Call<StandingList> call2, Response<StandingList> response) {

                if (!response.isSuccessful()) {
                    return;
                }

                StandingList sList = response.body();

                ArrayList<Standing> standings;
                standings = (ArrayList) sList.getStandings();
                ArrayList<Table> tables = new ArrayList<>();
                tables = (ArrayList) standings.get(0).getTable();

                for(Table tableItem : tables) {
                    String teamIdFromApi = String.valueOf(tableItem.getTeam().getId());
                    if(teamIdFromApi.equals(teamId)) {
                        team.setName(tableItem.getTeam().getName());
                        team.setPosition(tableItem.getPosition());
                        team.setPlayedGames(tableItem.getPlayedGames());
                        team.setPoints(tableItem.getPoints());
                        team.setGoalsFor(tableItem.getGoalsFor());
                        team.setGoalsAgainst(tableItem.getGoalsAgainst());
                        team.setCrestUrl((tableItem.getTeam().getCrestUrl()));
                    }
                }
                setTextFields(team);
            }

            @Override
            public void onFailure(Call<StandingList> call1, Throwable t) {
                Context context = getApplicationContext();
                CharSequence text = "Fel: " + t.getMessage();
                int duration = Toast.LENGTH_LONG;
                Toast toast = Toast.makeText(context, text, duration);
                toast.show();            }
        });

    }

    private void setTextFields(Team team) {

        TextView tw = findViewById(R.id.textView_team_name);
        t0 = team.getName();
        tw.setText(t0);

        tw = findViewById(R.id.textView_data_1);
        t1 = String.valueOf(team.getPosition());
        tw.setText(t1);

        tw = findViewById(R.id.textView_data_2);
        t2 = String.valueOf(team.getPlayedGames());
        tw.setText(t2);

        tw = findViewById(R.id.textView_data_3);
        t3 = String.valueOf(team.getPoints());
        tw.setText(t3);

        tw = findViewById(R.id.textView_data_4);
        t4 = String.valueOf(team.getGoalsFor());
        tw.setText(t4);

        tw = findViewById(R.id.textView_data_5);
        t5 = String.valueOf(team.getGoalsAgainst());
        tw.setText(t5);
    }

    @Override protected void onDestroy() {
        super.onDestroy();
        SvgLoader.pluck().close();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("tw0", t0);
        outState.putString("tw1", t1);
        outState.putString("tw2", t2);
        outState.putString("tw3", t3);
        outState.putString("tw4", t4);
        outState.putString("tw5", t5);
        outState.putString("url_svg", url_svg);


    }
}
