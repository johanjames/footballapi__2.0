package com.example.sqlitefotbol;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class FavouriteTeamWrapper {

    Team team;
    TextView textViewTeamName;
    ImageView imageViewAddButton;
    ImageView imageViewEditButton;
    ImageView imageViewDeleteButton;

    public FavouriteTeamWrapper(Team team) {
        this.team = team;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    public TextView getTextViewTeamName() {
        return textViewTeamName;
    }

    public void setTextViewTeamName(TextView textViewTeamName) {
        this.textViewTeamName = textViewTeamName;
    }

    public ImageView getImageViewAddButton() {
        return imageViewAddButton;
    }

    public void setImageViewAddButton(ImageView imageViewAddButton) {
        this.imageViewAddButton = imageViewAddButton;
    }

    public ImageView getImageViewEditButton() {
        return imageViewEditButton;
    }

    public void setImageViewEditButton(ImageView imageViewEditButton) {
        this.imageViewEditButton = imageViewEditButton;
    }

    public ImageView getImageViewDeleteButton() {
        return imageViewDeleteButton;
    }

    public void setImageViewDeleteButton(ImageView imageViewDeleteButton) {
        this.imageViewDeleteButton = imageViewDeleteButton;
    }
}


